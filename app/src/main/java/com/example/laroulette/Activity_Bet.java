package com.example.laroulette;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Activity_Bet extends AppCompatActivity {


    public static int moneybefore;
    public static int moneybet;

    public static List<String> bet = new ArrayList<String>();
    public static List<String> bet2 = new ArrayList<String>();
    String test="";
    int valeur_jeton = 1;
    int val1,val2,val3,val4,val5,val6,val7,val8,val9,val10,val11,val12,val13,val14,val15,val16,val17
            ,val18,val19,val20,val21,val22,val23,val24,val25,val26,val27,val28,val29,
            val30,val31,val32,val33,val34,val35,val36,valred,valblack,val0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bet);

        moneybefore= MainActivity.money;



        final Button back = (Button) findViewById(R.id.button_back);


        final TextView textview = (TextView) findViewById(R.id.textview_coor) ;

        final Button button_0 = (Button)findViewById(R.id.button_00_2);
        final Button button_1 = (Button)findViewById(R.id.button_1);
        final Button button_2 = (Button)findViewById(R.id.button_2);
        final Button button_3 = (Button)findViewById(R.id.button_3);
        final Button button_4 = (Button)findViewById(R.id.button_4);
        final Button button_5 = (Button)findViewById(R.id.button_5);
        final Button button_6 = (Button)findViewById(R.id.button_6);
        final Button button_7 = (Button)findViewById(R.id.button_7);
        final Button button_8 = (Button)findViewById(R.id.button_8);
        final Button button_9 = (Button)findViewById(R.id.button_9);
        final Button button_10= (Button)findViewById(R.id.button_10);
        final Button button_11= (Button)findViewById(R.id.button_11);
        final Button button_12= (Button)findViewById(R.id.button_12);
        final Button button_13= (Button)findViewById(R.id.button_13);
        final  Button button_14= (Button)findViewById(R.id.button_14);
        final Button button_15= (Button)findViewById(R.id.button_15);
        final   Button button_16= (Button)findViewById(R.id.button_16);
        final  Button button_17= (Button)findViewById(R.id.button_17);
        final     Button button_18= (Button)findViewById(R.id.button_18);
        final   Button button_19= (Button)findViewById(R.id.button_19);
        final   Button button_20= (Button)findViewById(R.id.button_20);
        final  Button button_21= (Button)findViewById(R.id.button_21);
        final   Button button_22= (Button)findViewById(R.id.button_22);
        final     Button button_23= (Button)findViewById(R.id.button_23);
        final    Button button_24= (Button)findViewById(R.id.button_24);
        final  Button button_25= (Button)findViewById(R.id.button_25);
        final  Button button_26= (Button)findViewById(R.id.button_26);
        final   Button button_27= (Button)findViewById(R.id.button_27);
        final   Button button_28= (Button)findViewById(R.id.button_28);
        final  Button button_29= (Button)findViewById(R.id.button_29);
        final   Button button_30= (Button)findViewById(R.id.button_30);
        final   Button button_31= (Button)findViewById(R.id.button_31);
        final   Button button_32= (Button)findViewById(R.id.button_32);
        final  Button button_33= (Button)findViewById(R.id.button_33);
        final  Button button_34= (Button)findViewById(R.id.button_34);
        final   Button button_35= (Button)findViewById(R.id.button_35);
        final  Button button_36= (Button)findViewById(R.id.button_36);

        final  Button button_red= (Button)findViewById(R.id.button_red);
        final  Button button_black= (Button)findViewById(R.id.button_black);

        ImageButton button_jetonrouge = (ImageButton)findViewById(R.id.button_jetonrouge);
        ImageButton button_jetonjaune = (ImageButton)findViewById(R.id.button_jetonjaune);
        ImageButton button_jetonbleu = (ImageButton)findViewById(R.id.button_jetonbleu);


        Button button_miser= (Button)findViewById(R.id.button_miser);
       final Button button_solde = (Button) findViewById(R.id.button_solde) ;

        button_solde.setText(MainActivity.money+"€");

        button_0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {                              //on regarde si on a assez d'argent pour parier

                    MainActivity.money-=valeur_jeton;                                  //on enlève l'argent qu'on parie
                    for (int j = 0; j < valeur_jeton; j++) {                            //on ajoute a un tableau chaque élement parié
                        bet.add("0");
                    }
                    val0+=valeur_jeton;
                    button_0.setText("0("+val0+"€)");                                   //on affiche le nombre d'euros parié sur ce bouton
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";

            }
        });
        //On fait pareil pour tous les autres boutons de la table de bet
        button_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("1 red");
                    }
                    val1+=valeur_jeton;
                    button_1.setText("1("+val1+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";

            }
        });

        button_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("2 black");
                    }
                    val2+=valeur_jeton;
                    button_2.setText("2("+val2+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("3 red");
                    }
                    val3+=valeur_jeton;
                    button_3.setText("3("+val3+"€)");

                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("4 black");
                    }
                    val4+=valeur_jeton;
                    button_4.setText("4("+val4+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("5 red");
                    }
                    val5+=valeur_jeton;
                    button_5.setText("5("+val5+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });
        button_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("6 black");
                    }
                    val6+=valeur_jeton;
                    button_6.setText("6("+val6+"€)");

                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("7 red");
                    }
                    val7+=valeur_jeton;
                    button_7.setText("7("+val7+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("8 black");
                    }
                    val8+=valeur_jeton;
                    button_8.setText("8("+val8+"€)");

                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("9 red");
                    }
                    val9+=valeur_jeton;
                    button_9.setText("9("+val9+"€)");

                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("10 black");
                    }
                    val10+=valeur_jeton;
                    button_10.setText("10("+val10+"€)");

                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("11 black");
                    }
                    val11+=valeur_jeton;
                    button_11.setText("11("+val11+"€)");

                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("12 red");
                    }
                    val12+=valeur_jeton;
                    button_12.setText("12("+val12+"€)");

                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("13 black");
                    }
                    val13+=valeur_jeton;
                    button_13.setText("13("+val13+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("14 red");
                    }
                    val14+=valeur_jeton;
                    button_14.setText("14("+val14+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("15 black");
                    }
                    val15+=valeur_jeton;
                    button_15.setText("15("+val15+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("16 red");
                    }
                    val16+=valeur_jeton;
                    button_16.setText("16("+val16+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("17 black");
                    }
                    val17+=valeur_jeton;
                    button_17.setText("17("+val17+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("18 red");
                    }
                    val18+=valeur_jeton;
                    button_18.setText("18("+val18+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_19.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("19 red");
                    }
                    val19+=valeur_jeton;
                    button_19.setText("19("+val19+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("20 black");
                    }
                    val20+=valeur_jeton;
                    button_20.setText("20("+val20+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("21 red");
                    }
                    val21+=valeur_jeton;
                    button_21.setText("21("+val21+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("22 black");
                    }
                    val22+=valeur_jeton;
                    button_22.setText("22("+val22+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("23 red");
                    }
                    val23+=valeur_jeton;
                    button_23.setText("23("+val23+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("24 black");
                    }
                    val24+=valeur_jeton;
                    button_24.setText("24("+val24+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });
        button_25.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("25 red");
                    }
                    val25+=valeur_jeton;
                    button_25.setText("25("+val25+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });
        button_26.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("26 black");
                    }
                    val26+=valeur_jeton;
                    button_26.setText("26("+val26+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_27.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("27 red");
                    }
                    val27+=valeur_jeton;
                    button_27.setText("27("+val27+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_28.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("28 black");
                    }
                    val28+=valeur_jeton;
                    button_28.setText("28("+val28+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_29.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("29 black");
                    }
                    val29+=valeur_jeton;
                    button_29.setText("29("+val29+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_30.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("30 red");
                    }
                    val30+=valeur_jeton;
                    button_30.setText("30("+val30+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("31 black");
                    }
                    val31+=valeur_jeton;
                    button_31.setText("31("+val31+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_32.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("32 red");
                    }
                    val32+=valeur_jeton;
                    button_32.setText("32("+val32+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_33.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("33 black");
                    }
                    val33+=valeur_jeton;
                    button_33.setText("33("+val33+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_34.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("34 red");
                    }
                    val34+=valeur_jeton;
                    button_34.setText("34("+val34+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_35.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("35 black");
                    }
                    val35+=valeur_jeton;
                    button_35.setText("35("+val35+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        button_36.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet.add("36 red");
                    }
                    val36+=valeur_jeton;
                    button_36.setText("36("+val36+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        //Bouton de  couleur, On ajoute dans une autre liste toutes les chiffres de la couleur red afin de voir si le résultat de la roulette se trouve dans cette liste.


        button_red.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet2.add("1 red");
                        bet2.add("3 red");
                        bet2.add("5 red");
                        bet2.add("7 red");
                        bet2.add("9 red");
                        bet2.add("12 red");
                        bet2.add("14 red");
                        bet2.add("16 red");
                        bet2.add("18 red");
                        bet2.add("19 red");
                        bet2.add("21 red");
                        bet2.add("23 red");
                        bet2.add("25 red");
                        bet2.add("27 red");
                        bet2.add("30 red");
                        bet2.add("32 red");
                        bet2.add("34 red");
                        bet2.add("36 red");

                    }
                    valred+=valeur_jeton;
                    button_red.setText("RED("+valred+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });

        //Idem que le bouton rouge

        button_black.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.money-valeur_jeton>=0) {

                    MainActivity.money-=valeur_jeton;
                    for (int j = 0; j < valeur_jeton; j++) {
                        bet2.add("2 black");
                        bet2.add("4 black");
                        bet2.add("6 black");
                        bet2.add("8 black");
                        bet2.add("10 black");
                        bet2.add("11 black");
                        bet2.add("13 black");
                        bet2.add("15 black");
                        bet2.add("17 black");
                        bet2.add("20 black");
                        bet2.add("22 black");
                        bet2.add("24 black");
                        bet2.add("26 black");
                        bet2.add("28 black");
                        bet2.add("29 black");
                        bet2.add("31 black");
                        bet2.add("33 black");
                        bet2.add("35 black");

                    }
                    valblack+=valeur_jeton;
                    button_black.setText("BLACK("+valblack+"€)");
                }

                button_solde.setText(MainActivity.money+"€");
                textview.setText(test);
                test ="";
            }
        });


        button_miser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moneybet = MainActivity.money;
                Intent i = new Intent(getApplicationContext(),ActivityGames.class);
                startActivity(i);
            }
        });


        //Jeton pour voir la quantité que l'on mise sur chaque bouton
        button_jetonrouge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valeur_jeton=1;
            }
        });

        button_jetonjaune.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valeur_jeton=10;
            }
        });

        button_jetonbleu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valeur_jeton=100;
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(i);
            }
        });

    }
    @Override
    protected void onPause() {
        super.onPause();
        setContentView(R.layout.activity_main);
        System.out.println(MainActivity.money);
        MainActivity.moneyedit.putInt("money", MainActivity.money);
        MainActivity.moneyedit.putInt("start",0);
        MainActivity.moneyedit.commit();
    }

}

































