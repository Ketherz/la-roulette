package com.example.laroulette;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;


//Adapteur custom pour la listeview


public class CustomAdapter extends ArrayAdapter<String> {
    private Context context;

    private ArrayList<String> numtab, datetab, statustab;
    private ArrayList<Integer> misetab;

    //Constructeur des cellules du tableau des scores

    public CustomAdapter(Context context, ArrayList<String> numtab, ArrayList<String> datetab, ArrayList<Integer> misetab, ArrayList<String> statustab)
    {
        super(context, R.layout.cell_layout, datetab);
        this.context = context;
        this.numtab = numtab;
        this.datetab = datetab;
        this.misetab = misetab;
        this.statustab = statustab;
    }

    //Code correspondant à une cellule du tableau des scores et son contenu 

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater =
                (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView= inflater.inflate(R.layout.cell_layout, null, true);

        TextView mise = (TextView) rowView.findViewById(R.id.mise);
        TextView date = (TextView) rowView.findViewById(R.id.date);
        TextView chiffre = (TextView) rowView.findViewById(R.id.num);
        TextView status = (TextView) rowView.findViewById(R.id.status);
        mise.setText(Integer.toString(misetab.get(position)));
        date.setText(datetab.get(position));
        chiffre.setText(numtab.get(position));
        status.setText(statustab.get(position));

        return rowView;
    }
}
