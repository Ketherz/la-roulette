package com.example.laroulette;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;




public class MainActivity extends AppCompatActivity {

    //SharedPreferences pour la persistance

    public static SharedPreferences misepref;
    public static SharedPreferences chiffrepref;
    public static SharedPreferences datepref;
    public static SharedPreferences statuspref;

    public static SharedPreferences.Editor miseedit;
    public static SharedPreferences.Editor chiffreedit;
    public static SharedPreferences.Editor dateedit;
    public static SharedPreferences.Editor statusedit;


    public static SharedPreferences moneypref; //Variable de persistance de money

    public static SharedPreferences.Editor moneyedit; //Editeur de money

    public static int money;



    Button play;
    Button score;
    Button resetmoney;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //on initialise les variables de persistances pour ne pas les écraser dans le futur

        misepref = getSharedPreferences("mise", 0);
        chiffrepref = getSharedPreferences("chiffre",0);
        datepref = getSharedPreferences("date",0);
        statuspref = getSharedPreferences("status", 0);

        miseedit = misepref.edit();
        chiffreedit = chiffrepref.edit();
        dateedit = datepref.edit();
        statusedit = statuspref.edit();

        //Récupération des tables de scores via sharedpref
        Activity_score.scorelistupdate(chiffrepref, misepref, datepref, statuspref);

        moneypref = getSharedPreferences("money", 0);


        moneyedit = moneypref.edit();

        if (money==0) {
            money = moneypref.getInt("money", 0);
            moneyedit.putInt("start",1);
            moneyedit.commit();
        }

        //mise en place des variables boutons

        play = (Button) findViewById(R.id.button);
        score = (Button) findViewById(R.id.button3);
        resetmoney = (Button) findViewById(R.id.button2) ;

        Button addfund = (Button)findViewById(R.id.button_money);

        //listener des différents boutons

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),ActivityGames.class);
                startActivity(i);
            }
        });


        score.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),Activity_score.class);
                startActivity(i);
            }
        });


        addfund.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),Activity_Money.class);
                startActivity(i);
            }
        });

        resetmoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                money = 0;
            }
        });

    }

    //mise à jour de l'argent dans la persistance

    @Override
    protected void onDestroy() {
        super.onDestroy();
        setContentView(R.layout.activity_main);
        System.out.println(money);
        moneyedit.putInt("money", money);
        moneyedit.putInt("start",0);
        moneyedit.commit();
    }

}
