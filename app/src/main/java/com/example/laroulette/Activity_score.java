package com.example.laroulette;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Date;

public class Activity_score extends AppCompatActivity {

    ListView liste = null;

    //Liste pour manipuler les sauvegardes

    public static ArrayList  mise = new ArrayList<Integer>();
    public static ArrayList date = new ArrayList<String>();
    public static ArrayList chiffre = new ArrayList<String>();
    public static ArrayList status = new ArrayList<String>();

    //SharedPreferences pour la persistance

    public static SharedPreferences misepref;
    public static SharedPreferences chiffrepref;
    public static SharedPreferences datepref;
    public static SharedPreferences statuspref;

    public static SharedPreferences.Editor miseedit;
    public static SharedPreferences.Editor chiffreedit;
    public static SharedPreferences.Editor dateedit;
    public static SharedPreferences.Editor statusedit;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        //Préparation des variables de persistance

        misepref = getSharedPreferences("mise", 0);
        chiffrepref = getSharedPreferences("chiffre",0);
        datepref = getSharedPreferences("date",0);
        statuspref = getSharedPreferences("status", 0);

        miseedit = misepref.edit();
        chiffreedit = chiffrepref.edit();
        dateedit = datepref.edit();
        statusedit = statuspref.edit();









        //récupération de la listeView
        liste = findViewById(R.id.listView);

        final CustomAdapter adapter=new CustomAdapter(Activity_score.this, chiffre,date, mise, status);
        liste.setAdapter(adapter);

        //fonction permettant la suppression des entrées des listes lors du maintient du click

        liste.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                mise.remove(i);
                date.remove(i);
                chiffre.remove(i);
                status.remove(i);
                liste.requestLayout();
                return true;
            }
        });



        }


    @Override
    protected void onPause() {
        super.onPause();

        //mise à jour de la persistance
        persistaceupdate(chiffreedit, dateedit, miseedit, statusedit);




    }

    //Fonction de mise à jour des listes via les sharedpref

    public static void scorelistupdate(SharedPreferences chiffrepref, SharedPreferences misepref, SharedPreferences datepref, SharedPreferences statuspref){
        if(mise.size()==0) {
            for (int i = 0; i < 10; i++) {
                if (!(chiffrepref.getString(Integer.toString(i), "Vide").equals("Vide"))) {
                    mise.add(misepref.getInt(Integer.toString(i), 0));
                    date.add(datepref.getString(Integer.toString(i), "Vide"));
                    chiffre.add(chiffrepref.getString(Integer.toString(i), "Vide"));
                    status.add(statuspref.getString(Integer.toString(i), "Vide"));
                }
            }
        }
    }

    //Fonction qui ajoute une entrée à l'historique

    public static void addEntry(Boolean statusunit, int miseunit, String chiffreunit){

        //Si le nombre d'entrée d'historique == 10 on retire la premiére entrée pour éviter l'affichage de trop d'entrées

        if(status.size()==10){
            status.remove(0);
            date.remove(0);
            mise.remove(0);
            chiffre.remove(0);
        }

        if (statusunit==true){
            status.add("Victoire");
        }
        else{
            status.add("Defaite");
        }

        Date d=new Date();

        //Les méthodes getDate() getMonth() et getYear() sont fonctionnelles même si deprecated

        date.add(d.getDate()+" / "+(d.getMonth()+1)+" / "+(d.getYear()+1900));
        mise.add(miseunit);
        chiffre.add(chiffreunit);
    }

    //permet de mettre à jour les tables de persistances

    public static void persistaceupdate(SharedPreferences.Editor chiffreedit, SharedPreferences.Editor dateedit, SharedPreferences.Editor miseedit, SharedPreferences.Editor statusedit ){
        for(int i = 0; i< mise.size(); i++){
            chiffreedit.putString(Integer.toString(i),(String) chiffre.get(i));
            dateedit.putString(Integer.toString(i),(String) date.get(i));
            miseedit.putInt(Integer.toString(i), (int) mise.get(i));
            statusedit.putString(Integer.toString(i),(String) status.get(i));
        }

        //On retire de la persistance les valeures supprimées

        for(int i = mise.size(); i<10; i++){
            chiffreedit.remove(Integer.toString(i));
            dateedit.remove(Integer.toString(i));
            miseedit.remove(Integer.toString(i));
            statusedit.remove(Integer.toString(i));
        }

        chiffreedit.commit();
        dateedit.commit();
        miseedit.commit();
        statusedit.commit();

    }



}
