package com.example.laroulette;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;


public class ActivityGames extends AppCompatActivity {

    Button buttonSpin;
    Button buttonBet;
    TextView textView;
    ImageView imroulette;
    Button back;


    public static int gain;
    public static String roulette_result;

    int nb=0;
    int nb2=0;

    Random r; //Variable pour tourner la roue aléatoirement

    int degree = 0, degree_old = 0;

    private static final String[] tabwheel={ "28 black ", "9 red", "26 black", " 30 red", "11 black",       //Tableau qui représente la roue dans l'ordre
     "7 red", "20 black", "32 red", "17 black", "5 red", "22 black", "34 red", "15 black", "3 red",
     "24 black", "36 red", "13 black", "1 red", "00", "27 red", "10 black", "25 red", "29 black",
     "12 red", "8 black", "19 red", "31 black", "18 red", "9 black", "21 red", "33 black", "16 red",
     "4 black", "23 red", "35 black", "14 red", "2 red", "0"};

    private static final float HALF_CASE = 360f / 38f / 2f; //On veut la moité d'une case, on divise 360 par le nombre de case et on redivise par 2


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        if(Activity_Bet.moneybefore==0){
            Activity_Bet.moneybefore=MainActivity.money;
        }

        buttonSpin =(Button) findViewById(R.id.button_Spin);
        buttonBet = (Button) findViewById(R.id.button_bet);
        textView = (TextView) findViewById(R.id.textView);
        back = (Button) findViewById(R.id.button_back);


        final TextView textview_results = (TextView) findViewById(R.id.textview_result);
        imroulette = (ImageView) findViewById(R.id.Roulette); //pas de majuscule pls
        final Button button_solde = (Button) findViewById(R.id.button_solde) ;

        r=new Random();

        button_solde.setText(MainActivity.money+"€");                       //On recupère le solde





        buttonSpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                degree_old=degree%360;
                degree = r.nextInt(3600)+1440;   // on rajoute 1440 pour que la roue fasse au moins 4 tours + le nombre random
                RotateAnimation rotate = new RotateAnimation(degree_old,degree,
                        RotateAnimation.RELATIVE_TO_SELF,0.5f,RotateAnimation.RELATIVE_TO_SELF,0.5f);
                rotate.setDuration(3600);
                rotate.setFillAfter(true);
                rotate.setInterpolator(new DecelerateInterpolator());
                rotate.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        textView.setText("");
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        textView.setText(getResult(360 - (degree % 360)));                          // affiche résulat de la roulette

                        for (int i = 0; i < Activity_Bet.bet.size(); i++) {                                 //parcours la liste des chiffres pariés


                            if (getResult(360 - (degree % 360)) == Activity_Bet.bet.get(i)) {
                                nb++;

                            }

                    }
                        for (int j = 0; j < Activity_Bet.bet2.size(); j++) {                                //parcours de la liste des couleurs pariés
                            if (getResult(360 - (degree % 360)) == Activity_Bet.bet2.get(j)) {
                                nb2++;


                            }
                        }

                        roulette_result=getResult(360 - (degree % 360));


                        MainActivity.money+=(nb*37);                                                //On multiplie la somme parié sur le bon chiffre par 37
                        MainActivity.money += (nb2 * 2);                                            //On multiplie la somme parié sur la bonne couleur par 2

                        Activity_Bet.bet.clear();               //on vide la liste
                        Activity_Bet.bet2.clear();


                        button_solde.setText(MainActivity.money+"€");                               //Affiche du solde

                        gain=MainActivity.money-Activity_Bet.moneybefore;                                   //calcul du gain




                        //affichage du gain ou de la perte
                        if(gain==0) {
                            textview_results.setText("Vous n'avez rien gagné et rien perdu :"+Integer.toString(gain)+"€");
                            Activity_score.addEntry(true,gain,roulette_result);
                            gain = 0;
                            Activity_Bet.moneybefore=MainActivity.money;
                        }

                        if(gain>0) {
                            textview_results.setText("Vous avez gagné "+Integer.toString(gain)+"€");
                            Activity_score.addEntry(true,gain,roulette_result);
                            gain = 0;
                            Activity_Bet.moneybefore=MainActivity.money;

                        }

                        if(gain<0) {
                            textview_results.setText("Vous avez perdu  "+Integer.toString(gain)+"€");
                            Activity_score.addEntry(false,gain,roulette_result);
                            gain = 0;
                            Activity_Bet.moneybefore=MainActivity.money;
                        }



                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                imroulette.startAnimation(rotate);   // Lance l'animation de la roulette.g
            }

        });

        buttonBet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),Activity_Bet.class);
                startActivity(i);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(i);
            }
        });
    }

    private String getResult(int degree){                   //fonction qui récupere le résultat de la roulette et le renvoi sous forme de String
        String result= null;

        for(int i=0;i<tabwheel.length && result==null;i++ ){
            float start = HALF_CASE*(i*2+1);
            float end = HALF_CASE*(i*2+3);

            if(degree >= start && degree<=end){
                result=tabwheel[i];
            }

        }

        return result;

    }
    //Sauvegarde dans les variables de persitances de l'argent et des historiques des que l'on quitte l'activitée
    @Override
    protected void onPause() {
        System.out.println("PAUSE");
        super.onPause();
        setContentView(R.layout.activity_main);
        MainActivity.moneyedit.putInt("money", MainActivity.money);
        MainActivity.moneyedit.putInt("start",0);
        MainActivity.moneyedit.commit();

        //mise à jour de la persistance
        Activity_score.persistaceupdate(MainActivity.chiffreedit, MainActivity.dateedit, MainActivity.miseedit, MainActivity.statusedit);



    }






}
