package com.example.laroulette;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Activity_Money extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_money);


        final EditText et=(EditText)findViewById(R.id.edittext_money);
        Button button_addfunds = (Button) findViewById(R.id.button_addfund);
        Button button_return = (Button) findViewById(R.id.button_return);
        final Button button_solde=(Button) findViewById(R.id.button_solde);

        final TextView textview_sommeadd = (TextView) findViewById(R.id.textview_sommeadd);
        final TextView textview_balance = (TextView) findViewById(R.id.textview_balance);

        button_solde.setText(MainActivity.money+"€");       //affiche le solde

        //bouton qui ajoute les fonds

        button_addfunds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.money = MainActivity.money+ Integer.parseInt(et.getText().toString());

                textview_sommeadd.setText("Vous avez ajouté "+et.getText().toString()+ "€");
                textview_balance.setText("Vous avez " + MainActivity.money+ "€");


                button_solde.setText(MainActivity.money+"€");



            }
        });

        button_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(i);
            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();
        setContentView(R.layout.activity_main);
        System.out.println(MainActivity.money);
        MainActivity.moneyedit.putInt("money", MainActivity.money);
        MainActivity.moneyedit.putInt("start",0);
        MainActivity.moneyedit.commit();
    }

}
